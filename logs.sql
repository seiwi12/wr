-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 23. Jan 2019 um 13:11
-- Server-Version: 10.1.35-MariaDB
-- PHP-Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `logs`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `eintraege`
--

CREATE TABLE `eintraege` (
  `id` int(11) NOT NULL,
  `variante` varchar(255) NOT NULL,
  `alterBetrag` varchar(255) NOT NULL,
  `neuerBetrag` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `eintraege`
--

INSERT INTO `eintraege` (`id`, `variante`, `alterBetrag`, `neuerBetrag`) VALUES
(203, 'Dollar2Euro', '22.0', '19.36'),
(204, 'Dollar2Yen', '22.0', '2403.06'),
(205, 'Dollar2Euro', '22.0', '19.36'),
(206, 'Dollar2Pfund', '22.0', '16.94'),
(207, 'Dollar2Euro', '22.43', '19.74'),
(208, 'Dollar2Yen', '28.0', '3058.44'),
(209, 'Dollar2Euro', '212.0', '186.56'),
(210, 'Dollar2Pfund', '223.32', '171.96'),
(211, 'Dollar2Euro', '22.43', '19.74'),
(212, 'Dollar2Yen', '28.0', '3058.44'),
(213, 'Dollar2Euro', '212.0', '186.56'),
(214, 'Dollar2Pfund', '223.32', '171.96'),
(215, 'Dollar2Euro', '22.43', '19.74'),
(216, 'Dollar2Yen', '28.0', '3058.44'),
(217, 'Dollar2Euro', '212.0', '186.56'),
(218, 'Dollar2Pfund', '223.32', '171.96'),
(219, 'Dollar2Euro', '22.43', '19.74'),
(220, 'Dollar2Yen', '28.0', '3058.44'),
(221, 'Dollar2Euro', '212.0', '186.56'),
(222, 'Dollar2Pfund', '223.32', '171.96'),
(223, 'Dollar2Euro', '22.43', '19.74'),
(224, 'Dollar2Yen', '28.0', '3058.44'),
(225, 'Dollar2Euro', '212.0', '186.56'),
(226, 'Dollar2Pfund', '223.32', '171.96'),
(227, 'Dollar2Euro', '22.43', '19.74'),
(228, 'Dollar2Yen', '28.0', '3058.44'),
(229, 'Dollar2Euro', '212.0', '186.56'),
(230, 'Dollar2Pfund', '223.32', '171.96'),
(231, 'Dollar2Euro', '22.43', '19.74'),
(232, 'Dollar2Yen', '28.0', '3058.44'),
(233, 'Dollar2Euro', '212.0', '186.56'),
(234, 'Dollar2Pfund', '223.32', '171.96'),
(235, 'Dollar2Euro', '22.43', '19.74'),
(236, 'Dollar2Yen', '28.0', '3058.44'),
(237, 'Dollar2Euro', '212.0', '186.56'),
(238, 'Dollar2Pfund', '223.32', '171.96'),
(239, 'Dollar2Euro', '22.43', '19.74'),
(240, 'Dollar2Yen', '28.0', '3058.44'),
(241, 'Dollar2Euro', '212.0', '186.56'),
(242, 'Dollar2Pfund', '223.32', '171.96'),
(243, 'Dollar2Euro', '22.43', '19.74'),
(244, 'Dollar2Yen', '28.0', '3058.44'),
(245, 'Dollar2Euro', '212.0', '186.56'),
(246, 'Dollar2Pfund', '223.32', '171.96'),
(247, 'Dollar2Euro', '22.43', '19.74'),
(248, 'Dollar2Yen', '28.0', '3058.44'),
(249, 'Dollar2Euro', '212.0', '186.56'),
(250, 'Dollar2Pfund', '223.32', '171.96'),
(251, 'Dollar2Euro', '22.43', '19.74'),
(252, 'Dollar2Yen', '28.0', '3058.44'),
(253, 'Dollar2Euro', '212.0', '186.56'),
(254, 'Dollar2Pfund', '223.32', '171.96'),
(255, 'Dollar2Euro', '22.43', '19.74'),
(256, 'Dollar2Yen', '28.0', '3058.44'),
(257, 'Dollar2Euro', '212.0', '186.56'),
(258, 'Dollar2Pfund', '223.32', '171.96'),
(259, 'Dollar2Euro', '22.43', '19.74'),
(260, 'Dollar2Yen', '28.0', '3058.44'),
(261, 'Dollar2Euro', '212.0', '186.56'),
(262, 'Dollar2Pfund', '223.32', '171.96'),
(263, 'Dollar2Euro', '22.43', '19.74'),
(264, 'Dollar2Yen', '28.0', '3058.44'),
(265, 'Dollar2Euro', '212.0', '186.56'),
(266, 'Dollar2Pfund', '223.32', '171.96'),
(267, 'Dollar2Euro', '22.43', '19.74'),
(268, 'Dollar2Yen', '28.0', '3058.44'),
(269, 'Dollar2Euro', '212.0', '186.56'),
(270, 'Dollar2Pfund', '223.32', '171.96'),
(271, 'Dollar2Euro', '22.43', '19.74'),
(272, 'Dollar2Yen', '28.0', '3058.44'),
(273, 'Dollar2Euro', '212.0', '186.56'),
(274, 'Dollar2Pfund', '223.32', '171.96');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `eintraege`
--
ALTER TABLE `eintraege`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `eintraege`
--
ALTER TABLE `eintraege`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=275;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
