package com.company.Adapter;

import com.company.Chain.WR;
import com.company.Interfaces.ISammelrechnung;

public class Sammelrechner implements ISammelrechnung {

    private WR wr;

    public Sammelrechner(WR wr){
        this.wr = wr;
    }
    @Override
    public double sammelumrechnen(double[] betraege, String variante) {
        double x =0;
       for (int i = 0; i < betraege.length; i++ ){
            x = x+ wr.umrechnen(variante,betraege[i]);

       }
       return x;
    }
}
