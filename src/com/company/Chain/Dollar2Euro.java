package com.company.Chain;

public class Dollar2Euro extends WR{

    public Dollar2Euro(){
        this.variante = "Dollar2Euro";
    }

    @Override
    protected double rechner(double betrag) {
        return round(betrag *0.88,2);
    }


}
