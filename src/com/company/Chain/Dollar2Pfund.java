package com.company.Chain;

public class Dollar2Pfund extends WR {

    public Dollar2Pfund(){
        this.variante = "Dollar2Pfund";
    }


    @Override
    protected double rechner(double betrag) {
        return round(betrag *0.77,2);
    }


}
