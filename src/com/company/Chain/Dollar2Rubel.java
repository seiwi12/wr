package com.company.Chain;

public class Dollar2Rubel extends WR{

    public Dollar2Rubel(){
        this.variante = "Dollar2Rubel";
    }


    @Override
    protected double rechner(double betrag) {
        return round(betrag *66.53,2);
    }

}
