package com.company.Chain;

import java.lang.Math.*;
public class Dollar2Yen extends WR {

    public Dollar2Yen(){
        this.variante = "Dollar2Yen";
    }

    @Override
    protected double rechner(double betrag) {
        return round(betrag *109.23,2);
    }


}
