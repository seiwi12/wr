package com.company.Chain;

import com.company.Command.LogObjekt;
import com.company.Decorator.Default;
import com.company.Decorator.MitGebuehr;
import com.company.Interfaces.IUmrechnen;

import java.util.Stack;

public abstract class WR implements IUmrechnen{

    protected String variante;
    protected WR nextWRrechner;
    private static double neuB = 0;
    private static LogObjekt logObjekt;

    private static Stack<LogObjekt> logObjekts = new Stack<>();
    public void setNextWRrechner(WR WRrechner){
        this.nextWRrechner = WRrechner;
    }
    public LogObjekt getLatestLogObjekt(){
        return logObjekt;
    }

    @Override
    public double umrechnen(String variante, double betrag) {

        if (this.variante == variante) {
            neuB = rechner(betrag);
            //neues Logobjekt
             this.logObjekt = new LogObjekt(variante,neuB,betrag);
             //Objekt wird in den Stack gespeichert
             log(logObjekt);
             //Decorator
             Default oG = new MitGebuehr(this);
             oG.print();
        }
        if (nextWRrechner != null){
              return nextWRrechner.umrechnen(variante,betrag);
        }
        return neuB;
    }

    abstract protected double rechner(double betrag);

    public double getBetrag(){
        return neuB;
    }
    public void setBetrag(double neuerBetrag){
        neuB = neuerBetrag;
    }
    public String getVariante(){
        return this.variante;
    }
    public void log(LogObjekt logObjekt){

           logObjekts.add(logObjekt);
    }
    public Stack<LogObjekt> getallLogs(){
       return logObjekts;
    }
    protected static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }






}
