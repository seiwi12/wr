package com.company.Command;

import sun.rmi.runtime.Log;

import java.util.Stack;

public class Aufrufer {

   public void Log(KonkreterBefehl konkreterBefehl,LogObjekt logObjekt){
      konkreterBefehl.log(logObjekt);
   }

   public Stack<LogObjekt> getLoggElements(KonkreterBefehl konkreterBefehl){
      return konkreterBefehl.getAllLogs();
   }
   public void redo(KonkreterBefehl konkreterBefehl){
      konkreterBefehl.redo();
   }
   public void undo(KonkreterBefehl konkreterBefehl){
      konkreterBefehl.undo();
   }
}
