package com.company.Command;

import com.company.Chain.WR;
import com.company.Interfaces.GeneralBefehl;
import com.company.Interfaces.IUmrechnen;

import java.util.Stack;

public class KonkreterBefehl implements GeneralBefehl {

   private WR wr;
   private double alterBetrag;
   public KonkreterBefehl(WR wr){
       this.wr = wr;
   }



    @Override
    public void log(LogObjekt logObjekt) {
        wr.log(logObjekt);
    }

    @Override
    public Stack<LogObjekt> getAllLogs() {
       this.alterBetrag = wr.getBetrag();
      return wr.getallLogs();
    }

    @Override
    public void undo() {
        wr.getallLogs().pop();
    }

    @Override
    public void redo() {
       LogObjekt logObjekt = wr.getallLogs().peek();
       wr.log(logObjekt);
    }
}
