package com.company.Command;

import sun.rmi.runtime.Log;

public class LogObjekt {
    private String variante;
    private double betrag;
    private double altbetrag;
    public void setVariante(String variante) {
        this.variante = variante;
    }

    public void setBetrag(double betrag) {
        this.betrag = betrag;
    }

    public String getVariante() {
        return variante;
    }

    public double getBetrag() {
        return betrag;
    }



    public void setAltbetrag(double altbetrag) {
        this.altbetrag = altbetrag;
    }

    public double getAltbetrag() {
        return altbetrag;
    }

    public LogObjekt(String variante, double betrag, double altbetrag){
        this.variante = variante;
        this.betrag = betrag;
        this.altbetrag = altbetrag;
    }
}
