package com.company.DAO;

import java.sql.DriverManager;

public class Connection {
    static java.sql.Connection conn = null;

    private Connection(){

    }
    public static java.sql.Connection getConn(){
        try {
            if(conn == null){
                conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/logs" , "root", "");

            }
        }catch(Exception exc){
            exc.printStackTrace();
        }
        return conn;
    }
}
