package com.company.DAO;

import com.company.Command.LogObjekt;
import com.company.Interfaces.LogDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class LogsDAOImpl implements LogDAO {
    Connection connection = com.company.DAO.Connection.getConn();
    @Override
    public void saveLogs(LogObjekt logObjekt) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO eintraege (id,variante,alterBetrag,neuerBetrag) VALUES (?,?,?,?);");
        preparedStatement.setString(1, null);
        preparedStatement.setString(2, logObjekt.getVariante());
        preparedStatement.setDouble(3, logObjekt.getAltbetrag());
        preparedStatement.setDouble(4,logObjekt.getBetrag() );
        preparedStatement.execute();
    }

    @Override
    public void deleteLog(int id) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("  DELETE FROM eintraege WHERE id=?;");
            preparedStatement.setInt(1, id);
            preparedStatement.execute();
        }catch (Exception exc){
            exc.printStackTrace();
        }
    }

    @Override
    public void getallLogsInDB() throws SQLException {
        ResultSet resultSet;
        PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM eintraege");
        resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            System.out.println("ID: " + resultSet.getInt("id") + " Variante: " + resultSet.getString("variante") + " Alter Betrag " + resultSet.getString("alterBetrag") + " Neuer Betrag " + resultSet.getString("neuerBetrag"));
        }
    }
}
