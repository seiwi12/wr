package com.company.Decorator;

import com.company.Chain.WR;
import com.company.Interfaces.GeneralLog;

public class Default implements GeneralLog {

    protected WR wr;

    public Default(WR wr){
        this.wr = wr;

    }


    @Override
    public void print() {
            System.out.println("Umrechenvorgang: " +wr.getVariante()+ " Mit Betrag: " +wr.getBetrag());
    }


}
