package com.company.Interfaces;

import com.company.Command.LogObjekt;

import java.util.Stack;

public interface GeneralBefehl{
    void log(LogObjekt logObjekt);
    Stack<LogObjekt> getAllLogs();
    void undo();
    void redo();

}
