package com.company.Interfaces;

public interface ISammelrechnung {
    public double sammelumrechnen(double[] betraege, String variante);
}
