package com.company.Interfaces;

import com.company.Command.LogObjekt;

import java.sql.SQLException;

public interface LogDAO {
     void saveLogs(LogObjekt logObjekt) throws SQLException;
     void deleteLog(int id);
     void getallLogsInDB() throws SQLException;

}
