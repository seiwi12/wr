package com.company;

import com.company.Adapter.Sammelrechner;
import com.company.Chain.*;
import com.company.Command.Aufrufer;
import com.company.Command.KonkreterBefehl;
import com.company.Command.LogObjekt;
import com.company.DAO.LogsDAOImpl;
import com.company.Decorator.MitGebuehr;
import com.company.Decorator.Default;
import sun.rmi.runtime.Log;

import java.sql.SQLException;

public class Main{


    public static void main(String[] args) {


		WR Dollar2Yen = new Dollar2Yen();
		WR Dollar2Euro = new Dollar2Euro();
		WR Dollar2Pfund = new Dollar2Pfund();
		WR Dollar2Rubel = new Dollar2Rubel();

		Dollar2Yen.setNextWRrechner(Dollar2Euro);
		Dollar2Euro.setNextWRrechner(Dollar2Pfund);
		Dollar2Pfund.setNextWRrechner(Dollar2Rubel);

		Dollar2Yen.umrechnen("Dollar2Euro",22.43);
		Dollar2Yen.umrechnen("Dollar2Yen",28.0);
		Dollar2Yen.umrechnen("Dollar2Euro",212.0);
		Dollar2Yen.umrechnen("Dollar2Pfund",223.32);


		KonkreterBefehl konkreterBefehl = new KonkreterBefehl(Dollar2Yen);
		Aufrufer aufrufer = new Aufrufer();
		LogsDAOImpl logsDAO= new LogsDAOImpl();
		/*
		System.out.println("----------------------------------Stack Vorher--------------------------------------------");
		for (LogObjekt logObjekt : aufrufer.getLoggElements(konkreterBefehl)){
			System.out.println(logObjekt.getVariante());
		}
		System.out.println("----------------------------------Stack Nachher--------------------------------------------");
		//Wiederholen ooder Loeschen
		aufrufer.redo(konkreterBefehl);
		aufrufer.redo(konkreterBefehl);
		aufrufer.undo(konkreterBefehl);
		aufrufer.undo(konkreterBefehl);
		for (LogObjekt logObjekt : aufrufer.getLoggElements(konkreterBefehl)){
			System.out.println(logObjekt.getVariante());
		}
		*/

		//Einzelner LogBefehl
		/*
		aufrufer.Log(konkreterBefehl,Dollar2Yen.getLatestLogObjekt());
		System.out.println(aufrufer.getLoggElements(konkreterBefehl).size());;
		*/

		//Delete DB Eintrag
		/*
		logsDAO.deleteLog(26);
		*/


		//Stack der Logs in die DB eintragen

		for (LogObjekt logObjekt : aufrufer.getLoggElements(konkreterBefehl)){
			try {
				logsDAO.saveLogs(logObjekt);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}


		//Get all Logs in DB
		System.out.println("----------------------------------Elemente in der Datenbank--------------------------------------");
		try {
			logsDAO.getallLogsInDB();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		//SammelRechnung
		System.out.println("----------------------------------Sammelrechnung-------------------------------------------------");

		double[] meinArray = {22.2,23.1,31.2,13.1,123.21};
		Sammelrechner sammelrechner = new Sammelrechner(Dollar2Yen);

		System.out.println("Summe "+sammelrechner.sammelumrechnen(meinArray,"Dollar2Pfund"));




	}
}
