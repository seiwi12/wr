package test.com.company.Chain; 

import com.company.Chain.*;
import org.junit.Test;
import org.junit.Before; 
import org.junit.After;
import static org.junit.Assert.assertEquals;

/** 
* WR Tester. 
* 
* @author <Stefan Seibl>
* @since <pre>J�n 22, 2019</pre> 
* @version 1.0 
*/ 
public class WRTest {
    WR Dollar2Yen = new Dollar2Yen();
    WR Dollar2Euro = new Dollar2Euro();
    WR Dollar2Pfund = new Dollar2Pfund();
    WR Dollar2Rubel = new Dollar2Rubel();
@Before
public void before() throws Exception {




    Dollar2Yen.setNextWRrechner(Dollar2Euro);
    Dollar2Euro.setNextWRrechner(Dollar2Pfund);
    Dollar2Pfund.setNextWRrechner(Dollar2Rubel);
} 

@After
public void after() throws Exception { 
} 

/** 
* 
* Method: setNextWRrechner(WR WRrechner) 
* 
*/ 
@Test
public void testSetNextWRrechner() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: getLatestLogObjekt() 
* 
*/ 
@Test
public void testGetLatestLogObjekt() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: umrechnen(String variante, double betrag) 
* 
*/ 
@Test
public void testUmrechnen() throws Exception {

    double erg3 = Dollar2Yen.umrechnen("Dollar2Rubel",2.2);
    assertEquals(146.37,erg3,0);


    double erg2 = Dollar2Yen.umrechnen("Dollar2Pfund",2.2);
    assertEquals(1.69,erg2,0);

    double erg1 = Dollar2Yen.umrechnen("Dollar2Yen",2.2);
    assertEquals(240.31,erg1,0);


    double erg = Dollar2Yen.umrechnen("Dollar2Euro",2.2);
    assertEquals(1.94,erg,0);


} 

/** 
* 
* Method: rechner(double betrag) 
* 
*/ 
@Test
public void testRechner() throws Exception {

} 

/** 
* 
* Method: getBetrag() 
* 
*/ 
@Test
public void testGetBetrag() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: setBetrag(double neuerBetrag) 
* 
*/ 
@Test
public void testSetBetrag() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: getVariante() 
* 
*/ 
@Test
public void testGetVariante() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: log(LogObjekt logObjekt) 
* 
*/ 
@Test
public void testLog() throws Exception { 
//TODO: Test goes here... 
} 

/** 
* 
* Method: getallLogs() 
* 
*/ 
@Test
public void testGetallLogs() throws Exception { 
//TODO: Test goes here... 
} 


} 
