package test.com.company.Command; 

import com.company.Chain.Dollar2Yen;
import com.company.Command.Aufrufer;
import com.company.Command.KonkreterBefehl;
import com.company.Command.LogObjekt;
import org.junit.Test;
import org.junit.Before; 
import org.junit.After;

import java.util.Stack;

import static org.junit.Assert.*;
import org.hamcrest.collection.IsEmptyCollection;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.hamcrest.MatcherAssert.assertThat;

/** 
* Aufrufer Tester. 
* 
* @author <Authors name> 
* @since <pre>J�n 23, 2019</pre> 
* @version 1.0 
*/ 
public class AufruferTest {
    Dollar2Yen dollar2Yen = new Dollar2Yen();
    KonkreterBefehl konkreterBefehl = new KonkreterBefehl(dollar2Yen);
    Aufrufer aufrufer = new Aufrufer();
    Stack<LogObjekt> stackA;


    @Before
public void before() throws Exception {
        aufrufer.Log(konkreterBefehl,new LogObjekt("Dollar2Yen",22,12));
        aufrufer.Log(konkreterBefehl,new LogObjekt("Dollar2Yen",22,12));
        aufrufer.Log(konkreterBefehl,new LogObjekt("Dollar2Yen",22,12));
        aufrufer.Log(konkreterBefehl,new LogObjekt("Dollar2Yen",22,12));
        stackA =  aufrufer.getLoggElements(konkreterBefehl);
} 

@After
public void after() throws Exception {

} 

/** 
* 
* Method: getLoggElements(KonkreterBefehl konkreterBefehl) 
* 
*/ 
@Test
public void testGetLoggElements() throws Exception {


    assertThat(stackA,hasSize(4));

} 

/** 
* 
* Method: redo(KonkreterBefehl konkreterBefehl) 
* 
*/ 
@Test
public void testRedo() throws Exception { 
    aufrufer.redo(konkreterBefehl);
    assertThat(stackA,hasSize(5));
} 

/** 
* 
* Method: undo(KonkreterBefehl konkreterBefehl) 
* 
*/ 
@Test
public void testUndo() throws Exception {
    aufrufer.undo(konkreterBefehl);
    assertThat(stackA,hasSize(3));
} 


} 
